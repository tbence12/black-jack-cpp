#include <iostream>
#include <string>
#include "jatekos.h"

using namespace std;


	Jatekos::Jatekos(string nev) {		//Default konstruktor
		this->jnev = nev;
	}

	Jatekos::Jatekos(string nev, int zseton) {		//Parameteres konstruktor
		this->jnev = nev;
		this->jzseton = zseton;
		this->jatszma = 1;
	}

	Jatekos::Jatekos(const Jatekos &jatekos) {		//Copy konstruktor
		this->jnev = jatekos.jnev;
		this->jzseton = jatekos.jzseton;
		this->jatszma = jatekos.jatszma;
	}


	Jatekos::~Jatekos() {		//Destruktor
	}

	void Jatekos::kartyaVizsgal(int x) {
		switch (x) {
		case 1: cout << " 1 ";
			break;
		case 2: cout << " 2 ";
			break;
		case 3: cout << " 3 ";
			break;
		case 4: cout << " 4 ";
			break;
		case 5: cout << " 5 ";
			break;
		case 6: cout << " 6 ";
			break;
		case 7: cout << " 7 ";
			break;
		case 8: cout << " 8 ";
			break;
		case 9: cout << " 9 ";
			break;
		case 10: cout << " 10/J/Q/K ";
			break;
		case 11: cout << " A ";
			break;
		default: cout << "valami nem jo!";
			break;
		}
	}

	int Jatekos::tetVizsgal(int x) {
		if (x != 50 || 100 || 150 || 250 || 500 || 1000) {
			cin.clear();											//HibakezelÚs
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			return 0;
		}
		else { return 1; }
	}

	string Jatekos::getJnev(){
		return this->jnev;
	}

	int Jatekos::getJatszma() {
		return this->jatszma;
	}

	int Jatekos::getJzseton(){
		return this->jzseton;
	}

	void Jatekos::setJzseton(int x){
		this->jzseton += x;
	}

	void Jatekos::operator++(int) {
		this->jatszma++;
	}

	std::ostream &operator<<(std::ostream &os, const Jatekos &jatekos) {  //operator overloading
		os << "Black Jack" << endl << endl << jatekos.jatszma << ". jatszma " << endl << endl << jatekos.jnev 
			<< " zsetonja: " << jatekos.jzseton << endl << endl;
		return os;
	}

	void Jatekos::jatekVege() {
			cout << endl << "Vege a jateknak!" << endl << "Sajnos elfogyott a zsetonod!" << endl << endl;
		}
	
	void Jatekos::zsetonGyujt(int x) {
		if (x > 15000) {
			cout << "Kezdesz feltuno lenni, lassan hagyd abba a jatekot!"<< endl;
		}
	}

	void Jatekos::jatszmaSok(int x) {
		if (x > 100) {
			cout << "Mar mondhatni fuggo vagy, allj le amig nem keso!" << endl;
		}
	}