#ifndef JATEKOS_H
#define JATEKOS_H

#include <iostream>
#include <string>

using namespace std;

class Jatekos {

	string jnev;
	int jzseton;
	int jatszma;

public:
	Jatekos(string);
	Jatekos(string, int);
	Jatekos(const Jatekos &jatekos);
	~Jatekos();
	void kartyaVizsgal(int);
	int tetVizsgal(int);
	string getJnev();
	int getJatszma();
	int getJzseton();
	void setJzseton(int);
	void operator++(int);															//operator
	friend std::ostream &operator<< (std::ostream &os, const Jatekos &jatekos);		//overloading
	void jatekVege();
	void zsetonGyujt(int);
	void jatszmaSok(int);



};

#endif